/**
 * StudentController
 *
 * @description :: Server-side logic for managing Students
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  new: function(req, res) {
    res.view()
  },
  create: function(req, res) {
    var StudentObj = {
      nombre: req.param('txtname')
    }
    Student.create(StudentObj, function(err, user) {
      if (err) {
        console.log(JSON.stringify(err));
        return res.redirect('/students');
      }
      res.redirect('/student');
    });
  }
};
