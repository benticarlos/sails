/**
 * ExamController
 *
 * @description :: Server-side logic for managing Exams
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  new: function(req, res) {
    res.view()
  },
  create: function(req, res) {
    var ExamObj = {
      name: req.param('txtname'),
      note: req.param('txtnote'),
      student: req.param('txtexam')
    }
    Exam.create(ExamObj, function(err, mesa) {
      if (err) {
        console.log(JSON.stringify(err));
      }
      Student.find(req.param('txtexam')).populate('exam').exec(function(err, users) {
        if (err) {
          console.log(JSON.stringify(err));
        }
        res.redirect('/student');
      });
    });

  },
  change: function(req, res) {
    Student.find({
      name: req.param('txtstudent')
    }).populateAll().exec(function(err, estud) {
      if (err) {
        console.log(err);
      } else {
        console.log(estud);
        estud.forEach(function(est) {
          Exam.find({
            student: est.id
          }).exec(function(err, es) {
            if (err) {
              console.log(err);
            }
            es.forEach(function(x) {
              if (x.note = req.param('txtnote'))
                Exam.update({
                  note: x.note
                }, {
                  note: req.param('txtnote2')
                }).exec(function afterwards(err, updated) {
                  if (err) {
                    console.log(err)
                  }
                });
            });
          });
        });
      }
    });
    res.redirect('/student');
  }
};
